//
//  HMCustomView.m
//  FBSnapshotTestCase
//
//  Created by zhm on 2018/12/21.
//

#import "HMCustomView.h"
#import "HMCustomLabel.h"

@interface HMCustomView ()

@end
@implementation HMCustomView


- (instancetype)initWithFrame:(CGRect)frame{
    
    self = [super initWithFrame:frame];
    if (self) {
        HMCustomLabel *label = [[HMCustomLabel alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        label.text = @"hello";
        label.textColor = [UIColor blueColor];
        [self addSubview:label];
    }
    
    return self;
}


@end

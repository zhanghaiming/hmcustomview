# HMCustomView

[![CI Status](https://img.shields.io/travis/张海明/HMCustomView.svg?style=flat)](https://travis-ci.org/张海明/HMCustomView)
[![Version](https://img.shields.io/cocoapods/v/HMCustomView.svg?style=flat)](https://cocoapods.org/pods/HMCustomView)
[![License](https://img.shields.io/cocoapods/l/HMCustomView.svg?style=flat)](https://cocoapods.org/pods/HMCustomView)
[![Platform](https://img.shields.io/cocoapods/p/HMCustomView.svg?style=flat)](https://cocoapods.org/pods/HMCustomView)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HMCustomView is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HMCustomView'
```

## Author

张海明, 1938708066@qq.com

## License

HMCustomView is available under the MIT license. See the LICENSE file for more info.

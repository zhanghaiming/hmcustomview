# HMCustomTagLabel

[![CI Status](https://img.shields.io/travis/张海明/HMCustomTagLabel.svg?style=flat)](https://travis-ci.org/张海明/HMCustomTagLabel)
[![Version](https://img.shields.io/cocoapods/v/HMCustomTagLabel.svg?style=flat)](https://cocoapods.org/pods/HMCustomTagLabel)
[![License](https://img.shields.io/cocoapods/l/HMCustomTagLabel.svg?style=flat)](https://cocoapods.org/pods/HMCustomTagLabel)
[![Platform](https://img.shields.io/cocoapods/p/HMCustomTagLabel.svg?style=flat)](https://cocoapods.org/pods/HMCustomTagLabel)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

HMCustomTagLabel is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'HMCustomTagLabel'
```

## Author

张海明, 1938708066@qq.com

## License

HMCustomTagLabel is available under the MIT license. See the LICENSE file for more info.

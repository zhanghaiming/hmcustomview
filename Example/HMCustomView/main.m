//
//  main.m
//  HMCustomView
//
//  Created by 张海明 on 12/21/2018.
//  Copyright (c) 2018 张海明. All rights reserved.
//

@import UIKit;
#import "HMAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([HMAppDelegate class]));
    }
}

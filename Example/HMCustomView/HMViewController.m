//
//  HMViewController.m
//  HMCustomView
//
//  Created by 张海明 on 12/21/2018.
//  Copyright (c) 2018 张海明. All rights reserved.
//

#import "HMViewController.h"
#import <HMCustomView/HMCustomView.h>
@interface HMViewController ()

@end

@implementation HMViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    HMCustomView *coustomView = [[HMCustomView alloc] initWithFrame:CGRectMake(0, 90, 200, 30)];
    [self.view addSubview:coustomView];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
